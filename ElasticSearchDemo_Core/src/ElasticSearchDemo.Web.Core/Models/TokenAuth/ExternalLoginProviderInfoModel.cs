﻿using Abp.AutoMapper;
using ElasticSearchDemo.Authentication.External;

namespace ElasticSearchDemo.Models.TokenAuth
{
    [AutoMapFrom(typeof(ExternalLoginProviderInfo))]
    public class ExternalLoginProviderInfoModel
    {
        public string Name { get; set; }

        public string ClientId { get; set; }
    }
}
