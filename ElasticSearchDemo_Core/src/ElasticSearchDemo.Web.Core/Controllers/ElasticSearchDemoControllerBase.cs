using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace ElasticSearchDemo.Controllers
{
    public abstract class ElasticSearchDemoControllerBase: AbpController
    {
        protected ElasticSearchDemoControllerBase()
        {
            LocalizationSourceName = ElasticSearchDemoConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
