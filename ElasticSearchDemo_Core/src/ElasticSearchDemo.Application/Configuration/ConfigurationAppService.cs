﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using ElasticSearchDemo.Configuration.Dto;

namespace ElasticSearchDemo.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : ElasticSearchDemoAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
