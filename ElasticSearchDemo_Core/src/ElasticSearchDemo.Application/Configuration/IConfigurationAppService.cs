﻿using System.Threading.Tasks;
using ElasticSearchDemo.Configuration.Dto;

namespace ElasticSearchDemo.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
