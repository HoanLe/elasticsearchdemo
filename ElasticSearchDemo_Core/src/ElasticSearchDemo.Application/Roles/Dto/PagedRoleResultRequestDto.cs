﻿using Abp.Application.Services.Dto;

namespace ElasticSearchDemo.Roles.Dto
{
    public class PagedRoleResultRequestDto : PagedResultRequestDto
    {
        public string Keyword { get; set; }
    }
}

