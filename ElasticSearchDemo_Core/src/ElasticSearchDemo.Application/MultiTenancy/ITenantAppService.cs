﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using ElasticSearchDemo.MultiTenancy.Dto;

namespace ElasticSearchDemo.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

