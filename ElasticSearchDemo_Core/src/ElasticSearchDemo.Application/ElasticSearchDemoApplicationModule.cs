﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using ElasticSearchDemo.Authorization;

namespace ElasticSearchDemo
{
    [DependsOn(
        typeof(ElasticSearchDemoCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class ElasticSearchDemoApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<ElasticSearchDemoAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(ElasticSearchDemoApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddProfiles(thisAssembly)
            );
        }
    }
}
