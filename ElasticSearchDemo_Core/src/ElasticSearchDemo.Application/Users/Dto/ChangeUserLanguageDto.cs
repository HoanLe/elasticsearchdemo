using System.ComponentModel.DataAnnotations;

namespace ElasticSearchDemo.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}