﻿using System.Threading.Tasks;
using Abp.Application.Services;
using ElasticSearchDemo.Authorization.Accounts.Dto;

namespace ElasticSearchDemo.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
