﻿using System.Threading.Tasks;
using Abp.Application.Services;
using ElasticSearchDemo.Sessions.Dto;

namespace ElasticSearchDemo.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
