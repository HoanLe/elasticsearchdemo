﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using ElasticSearchDemo.Configuration;

namespace ElasticSearchDemo.Web.Host.Startup
{
    [DependsOn(
       typeof(ElasticSearchDemoWebCoreModule))]
    public class ElasticSearchDemoWebHostModule: AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public ElasticSearchDemoWebHostModule(IHostingEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(ElasticSearchDemoWebHostModule).GetAssembly());
        }
    }
}
