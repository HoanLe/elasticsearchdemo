﻿namespace ElasticSearchDemo
{
    public class ElasticSearchDemoConsts
    {
        public const string LocalizationSourceName = "ElasticSearchDemo";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;
    }
}
