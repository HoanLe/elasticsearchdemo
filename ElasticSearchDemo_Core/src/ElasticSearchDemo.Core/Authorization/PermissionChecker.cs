﻿using Abp.Authorization;
using ElasticSearchDemo.Authorization.Roles;
using ElasticSearchDemo.Authorization.Users;

namespace ElasticSearchDemo.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
