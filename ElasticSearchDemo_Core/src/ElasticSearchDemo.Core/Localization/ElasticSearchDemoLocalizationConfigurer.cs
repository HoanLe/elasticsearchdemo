﻿using Abp.Configuration.Startup;
using Abp.Localization.Dictionaries;
using Abp.Localization.Dictionaries.Xml;
using Abp.Reflection.Extensions;

namespace ElasticSearchDemo.Localization
{
    public static class ElasticSearchDemoLocalizationConfigurer
    {
        public static void Configure(ILocalizationConfiguration localizationConfiguration)
        {
            localizationConfiguration.Sources.Add(
                new DictionaryBasedLocalizationSource(ElasticSearchDemoConsts.LocalizationSourceName,
                    new XmlEmbeddedFileLocalizationDictionaryProvider(
                        typeof(ElasticSearchDemoLocalizationConfigurer).GetAssembly(),
                        "ElasticSearchDemo.Localization.SourceFiles"
                    )
                )
            );
        }
    }
}
