﻿using Abp.MultiTenancy;
using ElasticSearchDemo.Authorization.Users;

namespace ElasticSearchDemo.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {            
        }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
        }
    }
}
