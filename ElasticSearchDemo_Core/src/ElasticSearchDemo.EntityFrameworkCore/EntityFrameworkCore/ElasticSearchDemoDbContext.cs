﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using ElasticSearchDemo.Authorization.Roles;
using ElasticSearchDemo.Authorization.Users;
using ElasticSearchDemo.MultiTenancy;

namespace ElasticSearchDemo.EntityFrameworkCore
{
    public class ElasticSearchDemoDbContext : AbpZeroDbContext<Tenant, Role, User, ElasticSearchDemoDbContext>
    {
        /* Define a DbSet for each entity of the application */
        
        public ElasticSearchDemoDbContext(DbContextOptions<ElasticSearchDemoDbContext> options)
            : base(options)
        {
        }
    }
}
