﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using ElasticSearchDemo.Configuration;
using ElasticSearchDemo.Web;

namespace ElasticSearchDemo.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class ElasticSearchDemoDbContextFactory : IDesignTimeDbContextFactory<ElasticSearchDemoDbContext>
    {
        public ElasticSearchDemoDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<ElasticSearchDemoDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            ElasticSearchDemoDbContextConfigurer.Configure(builder, configuration.GetConnectionString(ElasticSearchDemoConsts.ConnectionStringName));

            return new ElasticSearchDemoDbContext(builder.Options);
        }
    }
}
