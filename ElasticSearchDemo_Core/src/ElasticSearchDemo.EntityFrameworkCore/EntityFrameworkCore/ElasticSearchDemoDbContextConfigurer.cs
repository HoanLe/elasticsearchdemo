using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace ElasticSearchDemo.EntityFrameworkCore
{
    public static class ElasticSearchDemoDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<ElasticSearchDemoDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<ElasticSearchDemoDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}
